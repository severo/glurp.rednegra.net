---
layout: post
title: PostgreSQL y otras bases de datos
date: 2018-06-28 8:00:00 -0400
---


## A revisar

- [https://pgdash.io/blog/postgres-features.html](https://pgdash.io/blog/postgres-features.html)
- [https://www.infoq.com/presentations/future-distributed-database-relational](https://www.infoq.com/presentations/future-distributed-database-relational)
- [http://archiloque.net/weekly/18-07-01/](http://archiloque.net/weekly/18-07-01/)
- [http://workshops.boundlessgeo.com/postgis-intro/](http://workshops.boundlessgeo.com/postgis-intro/)
- [https://medium.com/@tjukanov/why-should-you-care-about-postgis-a-gentle-introduction-to-spatial-databases-9eccd26bc42b](https://medium.com/@tjukanov/why-should-you-care-about-postgis-a-gentle-introduction-to-spatial-databases-9eccd26bc42b)
- [https://martin.kleppmann.com/2015/05/11/please-stop-calling-databases-cp-or-ap.html](https://martin.kleppmann.com/2015/05/11/please-stop-calling-databases-cp-or-ap.html)
- [https://tarantool.io/en/developers/](https://tarantool.io/en/developers/)
