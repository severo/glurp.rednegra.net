---
layout: post
title: Soluciones federadas y P2P
date: 2018-06-28 8:00:00 -0400
---

## A revisar

- [https://twitter.com/coritoj](https://twitter.com/coritoj)
- [https://twitter.com/lubrio](https://twitter.com/lubrio)
- [https://twitter.com/dam1an](https://twitter.com/dam1an)
- [https://videos.lescommuns.org/about](https://videos.lescommuns.org/about)
- [http://www.internetactu.net/2015/12/01/avons-nous-besoin-dune-vitesse-limitee-sur-linternet/](http://www.internetactu.net/2015/12/01/avons-nous-besoin-dune-vitesse-limitee-sur-linternet/)
- [https://github.com/aphyr/distsys-class#dont-distribute](https://github.com/aphyr/distsys-class#dont-distribute)
- [https://mobile.twitter.com/aral/status/1006545310242562048](https://mobile.twitter.com/aral/status/1006545310242562048)
- [https://pfrazee.hashbase.io/blog/dat-and-servers](https://pfrazee.hashbase.io/blog/dat-and-servers)
- [https://mastodon.ar.al/@aral/100205160316195570](https://mastodon.ar.al/@aral/100205160316195570)
- [https://joinpeertube.org/en/home/](https://joinpeertube.org/en/home/)
- [https://syncthing.net/](https://syncthing.net/)
- [https://news.ycombinator.com/item?id=17303762](https://news.ycombinator.com/item?id=17303762)
- [https://forum.ind.ie/t/running-a-dat-share-as-a-service-with-systemctl-ubuntu-etc/2181](https://forum.ind.ie/t/running-a-dat-share-as-a-service-with-systemctl-ubuntu-etc/2181)
- [https://pixelfed.social/](https://pixelfed.social/)
- [https://blog.joinmastodon.org/2018/06/how-to-implement-a-basic-activitypub-server/](https://blog.joinmastodon.org/2018/06/how-to-implement-a-basic-activitypub-server/)
- [https://webtorrent.io/](https://webtorrent.io/)
- [https://code.eliotberriot.com/funkwhale/funkwhale/issues](https://code.eliotberriot.com/funkwhale/funkwhale/issues)
- [https://matrix.org/blog/home/](https://matrix.org/blog/home/)
- [https://gun.eco/](https://gun.eco/)
- [https://platform.coop/](https://platform.coop/)
- [https://indieweb.org/](https://indieweb.org/)
- [https://commotionwireless.net/](https://commotionwireless.net/)
- [https://mediagoblin.org/](https://mediagoblin.org/)
- [http://seenthis.net/messages/440318#message440318](http://seenthis.net/messages/440318#message440318)
- [https://tahoe-lafs.org/trac/tahoe-lafs](https://tahoe-lafs.org/trac/tahoe-lafs)
- [https://seenthis.net/tag/btsync](https://seenthis.net/tag/btsync)
- [https://unhosted.org/](https://unhosted.org/)
- [https://aaronparecki.com/2018/06/30/11/your-first-webmention](https://aaronparecki.com/2018/06/30/11/your-first-webmention)
