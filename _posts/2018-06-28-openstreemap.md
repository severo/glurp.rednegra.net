---
layout: post
title: OpenStreetMap
date: 2018-06-28 8:00:00 -0400
---

## A revisar

- [https://stevebennett.me/2017/08/23/openstreetmap-vector-tiles-mixing-and-matching-engines-schemas-and-styles/](https://stevebennett.me/2017/08/23/openstreetmap-vector-tiles-mixing-and-matching-engines-schemas-and-styles/)
- [https://github.com/gravitystorm/openstreetmap-carto](https://github.com/gravitystorm/openstreetmap-carto)
- [https://github.com/openmaptiles/openmaptiles](https://github.com/openmaptiles/openmaptiles)
- [https://www.tilehosting.com/#maps](https://www.tilehosting.com/#maps)
- [https://openmaptiles.com/](https://openmaptiles.com/)
- [https://github.com/osmlab/atlas-checks](https://github.com/osmlab/atlas-checks)
- [https://github.com/osmlab/atlas](https://github.com/osmlab/atlas)
- [https://overpass-turbo.eu/](https://overpass-turbo.eu/)
- [https://www.sites.univ-rennes2.fr/mastersigat/Cours/2018_SOTM_APIOverpass.pdf](https://www.sites.univ-rennes2.fr/mastersigat/Cours/2018_SOTM_APIOverpass.pdf)
- [https://github.com/osm-without-borders/cosmogony](https://github.com/osm-without-borders/cosmogony)
- [https://wiki.openstreetmap.org/wiki/OpenRouteService](https://wiki.openstreetmap.org/wiki/OpenRouteService)
- [https://github.com/posm/posm-replay-tool](https://github.com/posm/posm-replay-tool)
- [http://project-osrm.org/](http://project-osrm.org/)
- [https://geoffboeing.com/2016/11/osmnx-python-street-networks/ / https://github.com/gboeing/osmnx](https://geoffboeing.com/2016/11/osmnx-python-street-networks/ / https://github.com/gboeing/osmnx)
- [https://wiki.openstreetmap.org/wiki/ES:C%C3%B3mo*mapear...*(en_Bolivia)](<https://wiki.openstreetmap.org/wiki/ES:C%C3%B3mo_mapear..._(en_Bolivia)>)
- [https://wiki.openstreetmap.org/wiki/ES:Bolivia:Map_Features](https://wiki.openstreetmap.org/wiki/ES:Bolivia:Map_Features)
- [http://download.geofabrik.de/south-america/bolivia.html](http://download.geofabrik.de/south-america/bolivia.html)
- [https://github.com/openvenues/libpostal/blob/master/resources/boundaries/osm/bo.yaml](https://github.com/openvenues/libpostal/blob/master/resources/boundaries/osm/bo.yaml) / [https://en.wikipedia.org/wiki/Administrative_divisions_of_Bolivia](https://en.wikipedia.org/wiki/Administrative_divisions_of_Bolivia)
- [https://wiki.openstreetmap.org/wiki/Tag:boundary%3Dadministrative#11_admin_level_values_for_specific_countries](https://wiki.openstreetmap.org/wiki/Tag:boundary%3Dadministrative#11_admin_level_values_for_specific_countries)
- [https://journocode.com/2018/01/08/extract-geodata-openstreetmap-osmfilter/](https://journocode.com/2018/01/08/extract-geodata-openstreetmap-osmfilter/)
- [https://learnosm.org/en/osm-data/getting-data/](https://learnosm.org/en/osm-data/getting-data/)
- [https://wiki.openstreetmap.org/wiki/Processed_data_providers](https://wiki.openstreetmap.org/wiki/Processed_data_providers)
- [http://cosmogony.world/](http://cosmogony.world/) / [http://cosmogony.world/#/data_dashboard](http://cosmogony.world/#/data_dashboard)
- [https://osmcode.org/osmium-tool/](https://osmcode.org/osmium-tool/) - para extraer información de OSM, autoalojado
- [https://stadiamaps.com/](https://stadiamaps.com/)
- [https://github.com/simonepri/geo-maps](https://github.com/simonepri/geo-maps)
- [https://pic4review.pavie.info/#/](https://pic4review.pavie.info/#/)
- [https://github.com/adrianulbona/osm-parquetizer](https://github.com/adrianulbona/osm-parquetizer)
- [https://wiki.openstreetmap.org/wiki/Sophox](https://wiki.openstreetmap.org/wiki/Sophox)
- [https://osm.wikidata.link/](https://osm.wikidata.link/)
- [https://cualbondi.com.ar/](https://cualbondi.com.ar/)
- [https://www.caminosdelavilla.org/](https://www.caminosdelavilla.org/)
- [https://osm-pt.herokuapp.com/#map=12/-16.52074/-68.12408](https://osm-pt.herokuapp.com/#map=12/-16.52074/-68.12408)
- [https://github.com/osmlab/name-suggestion-index](https://github.com/osmlab/name-suggestion-index)
- [https://ohsome.org/](https://ohsome.org/)
- [http://magosm.magellium.com/](http://magosm.magellium.com/)
- [https://wiki.openstreetmap.org/wiki/OpenStreetMap:Wikibase](https://wiki.openstreetmap.org/wiki/OpenStreetMap:Wikibase)
- [https://commons.m.wikimedia.org/wiki/Commons:Structured_data/Get_involved/Feedback_requests/Search_prototype](https://commons.m.wikimedia.org/wiki/Commons:Structured_data/Get_involved/Feedback_requests/Search_prototype)
- [https://osm-vis.geog.uni-heidelberg.de/](https://osm-vis.geog.uni-heidelberg.de/)

Extracción de datos:

- [https://github.com/simonepri/geo-maps/blob/master/README.md](https://github.com/simonepri/geo-maps/blob/master/README.md)
