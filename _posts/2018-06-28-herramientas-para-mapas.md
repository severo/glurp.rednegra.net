---
layout: post
title: Herramientas para mapas
date: 2018-06-28 8:00:00 -0400
---

## A revisar

- [http://mapshaper.org/ /
  https://github.com/mbloch/mapshaper](http://mapshaper.org/ /
  https://github.com/mbloch/mapshaper)
- [https://devseed.com/dirty-reprojectors-app/](https://devseed.com/dirty-reprojectors-app/)
- [https://bl.ocks.org/mbostock](https://bl.ocks.org/mbostock)
- [https://jakearchibald.github.io/svgomg/](https://jakearchibald.github.io/svgomg/)
- [http://dropchop.io/](http://dropchop.io/)
- [http://geotiff.io/](http://geotiff.io/)
- [http://geojson.io/#map=2/20.0/0.0](http://geojson.io/#map=2/20.0/0.0)
- [http://geojsonlint.com/](http://geojsonlint.com/)
- [http://bboxfinder.com/#0.000000,0.000000,0.000000,0.000000](http://bboxfinder.com/#0.000000,0.000000,0.000000,0.000000)
- [http://projectionwizard.org/](http://projectionwizard.org/)
- [listas públicas
  (https://mobile.twitter.com/TheMapSmith/status/976166015720620032)](listas
  públicas (https://mobile.twitter.com/TheMapSmith/status/976166015720620032))
- [https://tangram.city/play/#3.00/0.00/0.00](https://tangram.city/play/#3.00/0.00/0.00)
- [http://www.h2gis.org/docs/dev/ST_SVF/](http://www.h2gis.org/docs/dev/ST_SVF/)
- [http://thinkhazard.org/es/](http://thinkhazard.org/es/)
- [https://mobile.twitter.com/EscuelaDeDatos/status/978718627036893185](https://mobile.twitter.com/EscuelaDeDatos/status/978718627036893185)
- [https://beta.observablehq.com/@mbostock/changelog-2018-03-28](https://beta.observablehq.com/@mbostock/changelog-2018-03-28)
- [http://www.digital-democracy.org/mapeo/](http://www.digital-democracy.org/mapeo/)
- [GeoPoppy
  (https://osgeo-fr.github.io/presentations_foss4gfr/2016/J1/FOSS4G-fr_INRA_JA_GeoPoppy.pdf](GeoPoppy
  (https://osgeo-fr.github.io/presentations_foss4gfr/2016/J1/FOSS4G-fr_INRA_JA_GeoPoppy.pdf)
- [https://github.com/jancelin/geo-poppy)](https://github.com/jancelin/geo-poppy))
- [http://blockbuilder.org/](http://blockbuilder.org/)
- [https://jupyter.org/](https://jupyter.org/)
- [https://nteract.io/atom](https://nteract.io/atom)
- [https://datavis.tech/](https://datavis.tech/)
- [https://beta.observablehq.com/](https://beta.observablehq.com/)
- [https://github.com/tomnomnom/gron/](https://github.com/tomnomnom/gron/)
- [https://www.jsonstore.io/](https://www.jsonstore.io/)
- [https://github.com/dinedal/textql](https://github.com/dinedal/textql)
- [http://projects.susielu.com/viz-palette](http://projects.susielu.com/viz-palette)
- [http://www.data-illustrator.com/](http://www.data-illustrator.com/)
- [https://osm.wikidata.link/](https://osm.wikidata.link/)
- [https://github.com/mapbox/rasterio](https://github.com/mapbox/rasterio)
- [https://github.com/mapbox/polylabel](https://github.com/mapbox/polylabel)
- [http://mapnik.org/](http://mapnik.org/)
- [https://geocode.localfocus.nl/](https://geocode.localfocus.nl/)
- [http://geoexamples.com/other/2018/04/30/mapping-with-gpujs.html](http://geoexamples.com/other/2018/04/30/mapping-with-gpujs.html)
- [https://beta.observablehq.com/@tmcw/javascript-replacements-for-python-data-science-tools](https://beta.observablehq.com/@tmcw/javascript-replacements-for-python-data-science-tools)
- [https://github.com/TomNomNom/gron](https://github.com/TomNomNom/gron)
- [https://www.jawg.io/en/](https://www.jawg.io/en/)
- [https://simonwillison.net/2018/May/20/datasette-facets/](https://simonwillison.net/2018/May/20/datasette-facets/)
- [https://github.com/tilezen/joerd](https://github.com/tilezen/joerd)
- [https://mobile.twitter.com/nicolaraluk/status/1000803507019177985](https://mobile.twitter.com/nicolaraluk/status/1000803507019177985)
- [https://www.journaldunet.com/solutions/seo-referencement/1209643-11-alternatives-a-google-maps-leur-cout-leurs-avantages/](https://www.journaldunet.com/solutions/seo-referencement/1209643-11-alternatives-a-google-maps-leur-cout-leurs-avantages/)
- [https://seenthis.net/messages/697682](https://seenthis.net/messages/697682)
- [https://seenthis.net/messages/563323](https://seenthis.net/messages/563323)
- [http://datameet.org/2018/06/13/openrefine-bus-stop/](http://datameet.org/2018/06/13/openrefine-bus-stop/)
- [https://copernix.io/#?where=-68.12172779356024,-16.498831196149833,17&?query=la%20paz](https://copernix.io/#?where=-68.12172779356024,-16.498831196149833,17&?query=la%20paz)
- [https://medium.com/@LoicOrtola/mapocalypse-migrer-depuis-google-maps-maintenant-2-2-e4359112e20a](https://medium.com/@LoicOrtola/mapocalypse-migrer-depuis-google-maps-maintenant-2-2-e4359112e20a)
- [https://medium.com/@alark/we-need-more-interactive-data-visualization-tools-for-the-web-in-python-ad80ec3f440e](https://medium.com/@alark/we-need-more-interactive-data-visualization-tools-for-the-web-in-python-ad80ec3f440e)
- [https://uber.github.io/kepler.gl/#/](https://uber.github.io/kepler.gl/#/)
- [https://github.com/osmlab/atlas](https://github.com/osmlab/atlas)
- [https://github.com/teralytics/flowmap.gl](https://github.com/teralytics/flowmap.gl)
- [https://github.com/syt123450/giojs](https://github.com/syt123450/giojs)
- [https://mobile.twitter.com/pa_chevalier/status/1014067478371209217](https://mobile.twitter.com/pa_chevalier/status/1014067478371209217)
- [https://mobile.twitter.com/datachile_io/status/1013802257937297411](https://mobile.twitter.com/datachile_io/status/1013802257937297411)
- [https://mappinggis.com/2018/07/5-herramientas-de-color-simbologia-mapas/](https://mappinggis.com/2018/07/5-herramientas-de-color-simbologia-mapas/)
- [https://docs.google.com/spreadsheets/d/1k-GVrhLw2_KdSvfCOt3RzF6otWfZQ5rbsBpVnTP425w/htmlview](https://docs.google.com/spreadsheets/d/1k-GVrhLw2_KdSvfCOt3RzF6otWfZQ5rbsBpVnTP425w/htmlview)
- [https://www.movable-type.co.uk/scripts/geohash.html](https://www.movable-type.co.uk/scripts/geohash.html)
- [https://uwdata.github.io/draco/](https://uwdata.github.io/draco/)
- [https://seenthis.net/messages/708793 /
  https://blog.mapbox.com/visualizing-street-orientations-on-an-interactive-map-1eefa6002afc
  / https://mourner.github.io/road-orientation-map/ /
  http://geoffboeing.com/2018/07/comparing-city-street-orientations /
  http://geoffboeing.com/2018/07/city-street-orientations-world](https://seenthis.net/messages/708793
  /
  https://blog.mapbox.com/visualizing-street-orientations-on-an-interactive-map-1eefa6002afc
  / https://mourner.github.io/road-orientation-map/ /
  http://geoffboeing.com/2018/07/comparing-city-street-orientations /
  http://geoffboeing.com/2018/07/city-street-orientations-world)
- [https://two.js.org/](https://two.js.org/)
- [https://seenthis.net/messages/193519](https://seenthis.net/messages/193519)
- [https://github.com/mapbox/carmen](https://github.com/mapbox/carmen)
- [https://www.digital-geography.com/data-extraction-and-clean-up-with-tabula-and-open-refine/](https://www.digital-geography.com/data-extraction-and-clean-up-with-tabula-and-open-refine/)
- [https://github.com/ashwin711/georaptor/blob/master/README.rst,
  https://github.com/ashwin711,
  https://github.com/soorajb/loc2country/blob/master/README.md](https://github.com/ashwin711/georaptor/blob/master/README.rst,
  https://github.com/ashwin711,
  https://github.com/soorajb/loc2country/blob/master/README.md)
- [http://js.cytoscape.org/](http://js.cytoscape.org/)
- [https://maputnik.github.io/ /
  https://mobile.twitter.com/gonzalo_lpgc/status/1021426695486824448 /
  https://maputnik.github.io/blog/2018/07/12/release-v1.3.0](https://maputnik.github.io/
  / https://mobile.twitter.com/gonzalo_lpgc/status/1021426695486824448 /
  https://maputnik.github.io/blog/2018/07/12/release-v1.3.0)
- [https://github.com/mljs/ml](https://github.com/mljs/ml)
- [https://mobile.twitter.com/Info_Activismo/status/1022537784479735808 /
  https://www.canva.com/color-palette/ / https://coolors.co/ /
  https://color.adobe.com/es/create/color-wheel/](https://mobile.twitter.com/Info_Activismo/status/1022537784479735808
  / https://www.canva.com/color-palette/ / https://coolors.co/ /
  https://color.adobe.com/es/create/color-wheel/)
- [https://rawgraphs.io/](https://rawgraphs.io/)
- [http://www.joelsimon.net/evo_floorplans.html](http://www.joelsimon.net/evo_floorplans.html)
- [http://www.pixijs.com/](http://www.pixijs.com/)
- [https://beta.observablehq.com/@larsvers/hexgrid-maps-with-d3-hexgrid](https://beta.observablehq.com/@larsvers/hexgrid-maps-with-d3-hexgrid)
- [https://bitbucket.org/pintodante/extractor/src/master/](https://bitbucket.org/pintodante/extractor/src/master/)
- [https://ptsjs.org/](https://ptsjs.org/)
- [https://github.com/tidwall/tile38](https://github.com/tidwall/tile38)
- [https://flourish.studio/](https://flourish.studio/)
- [https://charticulator.com/](https://charticulator.com/)
- [http://www.data-illustrator.com/](http://www.data-illustrator.com/)
- [https://github.com/Geovation/labelgun](https://github.com/Geovation/labelgun)
- [https://d3plus.org/examples/](https://d3plus.org/examples/)
- [https://twitter.com/klokantech/status/1042789226427740162](https://twitter.com/klokantech/status/1042789226427740162)
- [http://faremap.ml/](http://faremap.ml/)
- [https://github.com/noncomputable/AgentMaps](https://github.com/noncomputable/AgentMaps)
- [https://github.com/neogeo-technologies/OneTjs/blob/master/README.md](https://github.com/neogeo-technologies/OneTjs/blob/master/README.md)
- [Distance calculator](https://www.distance.to/)

Geocoding:

- [http://getlon.lat/](http://getlon.lat/)

BigData:

- [https://www.geomesa.org/](https://www.geomesa.org/)

Límites territoriales:

- [https://github.com/etalab/geozones](https://github.com/etalab/geozones)
