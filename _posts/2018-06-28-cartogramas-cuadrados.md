---
layout: post
title: Cartogramas / grid maps
date: 2018-06-28 8:00:00 -0400
---

## A revisar

- [https://rud.is/b/2018/06/07/making-world-tile-grid-grids/](https://rud.is/b/2018/06/07/making-world-tile-grid-grids/)
- [https://mobile.twitter.com/tjukanov/status/1011308061892505602](https://mobile.twitter.com/tjukanov/status/1011308061892505602)
- [http://www.maartenlambrechts.com/2017/10/22/tutorial-a-worldtilegrid-with-ggplot2.html](http://www.maartenlambrechts.com/2017/10/22/tutorial-a-worldtilegrid-with-ggplot2.html)
- [http://datavizcatalogue.com/blog/chart-combinations-tile-grid-maps/](http://datavizcatalogue.com/blog/chart-combinations-tile-grid-maps/)
- [https://beta.observablehq.com/@severo/grid-cartograms](https://beta.observablehq.com/@severo/grid-cartograms)
- [https://mobile.twitter.com/ProfDrKSchulte/status/1015278083375554560](https://mobile.twitter.com/ProfDrKSchulte/status/1015278083375554560)
- [https://mobile.twitter.com/LazaroGamio/status/1020383163401539584 / https://www.axios.com/trump-tariffs-states-midterms-51ea2753-0c3e-438d-b0e9-684816d23893.html](https://mobile.twitter.com/LazaroGamio/status/1020383163401539584 / https://www.axios.com/trump-tariffs-states-midterms-51ea2753-0c3e-438d-b0e9-684816d23893.html)
- [https://www.gicentre.net/featuredpapers/#/literate2018/](https://www.gicentre.net/featuredpapers/#/literate2018/)
- [https://m.g3l.org/users/meteo_fr_metro/statuses/100434700388610262](https://m.g3l.org/users/meteo_fr_metro/statuses/100434700388610262)
- [http://code.minnpost.com/aranger/](http://code.minnpost.com/aranger/)
- [https://github.com/wsj/squaire](https://github.com/wsj/squaire)
- [https://pitchinteractiveinc.github.io/tilegrams/](https://pitchinteractiveinc.github.io/tilegrams/)
- [https://beta.observablehq.com/@jeremiak/an-animated-cartogram-of-the-us-congress](https://beta.observablehq.com/@jeremiak/an-animated-cartogram-of-the-us-congress)
- [https://github.com/larsvers/d3-hexgrid / https://bl.ocks.org/larsvers/049c8f382ea07d48ca0a395e661d0fa4 / https://beta.observablehq.com/@larsvers/d3-hexgrid-examples / https://beta.observablehq.com/@larsvers/hexgrid-maps-with-d3-hexgrid / https://www.redblobgames.com/grids/hexagons/](https://github.com/larsvers/d3-hexgrid / https://bl.ocks.org/larsvers/049c8f382ea07d48ca0a395e661d0fa4 / https://beta.observablehq.com/@larsvers/d3-hexgrid-examples / https://beta.observablehq.com/@larsvers/hexgrid-maps-with-d3-hexgrid / https://www.redblobgames.com/grids/hexagons/)
- [https://rud.is/b/2018/08/27/simplifying-world-tile-grid-creation-with-geom_wtg/](https://rud.is/b/2018/08/27/simplifying-world-tile-grid-creation-with-geom_wtg/)
- [https://ourworldindata.org/wp-content/uploads/2018/09/Population-cartogram_World.png](https://ourworldindata.org/wp-content/uploads/2018/09/Population-cartogram_World.png) / [https://ourworldindata.org/world-population-cartogram](https://ourworldindata.org/world-population-cartogram)
- [http://ici.radio-canada.ca/special/2018/elections-quebec/circonscriptions-resultats-cartes-vote-analyse-politique/](http://ici.radio-canada.ca/special/2018/elections-quebec/circonscriptions-resultats-cartes-vote-analyse-politique/)
