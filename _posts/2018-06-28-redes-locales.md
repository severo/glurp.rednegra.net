---
layout: post
title: Redes locales
date: 2018-06-28 8:00:00 -0400
---

## A revisar

- [https://www.internetsociety.org/es/blog/2018/03/construccion-de-infraestructura-comunitaria/](https://www.internetsociety.org/es/blog/2018/03/construccion-de-infraestructura-comunitaria/)
- [https://www.internetsociety.org/blog/2018/05/innovative-licensing-approaches-enabling-access-in-hard-to-reach-places-through-collaborative-partnerships/](https://www.internetsociety.org/blog/2018/05/innovative-licensing-approaches-enabling-access-in-hard-to-reach-places-through-collaborative-partnerships/)
- [https://globalvoices.org/2018/05/01/in-mexico-an-indigenous-community-telco-will-continue-to-operate-for-now/](https://globalvoices.org/2018/05/01/in-mexico-an-indigenous-community-telco-will-continue-to-operate-for-now/)
- [http://blog.altermundi.net/article/diplomado-comunitario-de-promotores-en-telecomunic/](http://blog.altermundi.net/article/diplomado-comunitario-de-promotores-en-telecomunic/)
- [http://www.altermundi.net/colabora](http://www.altermundi.net/colabora)
- [http://altermundi.net/](http://altermundi.net/)
- [https://wiki.openstreetmap.org/wiki/FR:Proposed_features/Telecom_local_loop](https://wiki.openstreetmap.org/wiki/FR:Proposed_features/Telecom_local_loop)
- [https://librerouter.org/article/the-librerouter-is-almost-out-who-wants-one/](https://librerouter.org/article/the-librerouter-is-almost-out-who-wants-one/)
- [https://rising.globalvoices.org/blog/2018/06/20/south-africa-becomes-the-first-country-in-the-region-to-support-community-networks/](https://rising.globalvoices.org/blog/2018/06/20/south-africa-becomes-the-first-country-in-the-region-to-support-community-networks/)
- [http://researchrepository.murdoch.edu.au/id/eprint/3982/1/Comparison_of_Routing_Protocols.pdf](http://researchrepository.murdoch.edu.au/id/eprint/3982/1/Comparison_of_Routing_Protocols.pdf)
- [https://blog.cloudflare.com/cloudflare-argo-tunnel-with-rust-and-raspberry-pi/](https://blog.cloudflare.com/cloudflare-argo-tunnel-with-rust-and-raspberry-pi/)
- [https://rmgss.net/posts/LaOtraRed-mayo-2018](https://rmgss.net/posts/LaOtraRed-mayo-2018)
- [https://desec.io/](https://desec.io/)
- [https://lapaz.laotrared.net/](https://lapaz.laotrared.net/)
- [https://www.youtube.com/watch?v=sZX89gTM12M&feature=youtu.be&app=desktop](https://www.youtube.com/watch?v=sZX89gTM12M&feature=youtu.be&app=desktop)
- [https://plus.google.com/u/0/+InternetSocietyAm%C3%A9ricaLatinayelCaribeOk/posts/FLLuZdizSNM](https://plus.google.com/u/0/+InternetSocietyAm%C3%A9ricaLatinayelCaribeOk/posts/FLLuZdizSNM)
- [https://mobile.twitter.com/pierky/status/758989219519791104 / https://www.pierky.com/ripeatlastracepath/demo/](https://mobile.twitter.com/pierky/status/758989219519791104 / https://www.pierky.com/ripeatlastracepath/demo/)
- [https://www.internetsociety.org/blog/2018/05/community-networks-can-bridge-the-digital-divide-but-some-still-need-to-be-convinced/](https://www.internetsociety.org/blog/2018/05/community-networks-can-bridge-the-digital-divide-but-some-still-need-to-be-convinced/)
- [https://mobile.twitter.com/pierky/status/758989219519791104 / https://www.pierky.com/ripeatlastracepath/demo/](https://mobile.twitter.com/pierky/status/758989219519791104 / https://www.pierky.com/ripeatlastracepath/demo/)
- [https://www.internetsociety.org/blog/2018/05/community-networks-can-bridge-the-digital-divide-but-some-still-need-to-be-convinced/](https://www.internetsociety.org/blog/2018/05/community-networks-can-bridge-the-digital-divide-but-some-still-need-to-be-convinced/)
- [http://sg-pub.ripe.net/ixp-country-jedi/bo/2018/03/01/](http://sg-pub.ripe.net/ixp-country-jedi/bo/2018/03/01/)
- [https://mobile.twitter.com/Rizomias/status/1042583601274085376](https://mobile.twitter.com/Rizomias/status/1042583601274085376)
- [https://mobile.twitter.com/Bocadepolen/status/1042482173297795072](https://mobile.twitter.com/Bocadepolen/status/1042482173297795072)
- [https://notas.lapaz.laotrared.net/s/BktNCEgYQ](https://notas.lapaz.laotrared.net/s/BktNCEgYQ)
