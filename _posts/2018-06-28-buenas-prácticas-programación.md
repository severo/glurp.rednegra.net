---
layout: post
title: Buenas prácticas de programación
date: 2018-06-28 8:00:00 -0400
---


## A revisar

- [https://itnext.io/dynamic-programming-vs-divide-and-conquer-2fea680becbe](https://itnext.io/dynamic-programming-vs-divide-and-conquer-2fea680becbe)
- [https://12factor.net/port-binding](https://12factor.net/port-binding)
- [https://volumio.github.io/docs/Development_How_To/System_Architecture.html, weighted round robin](https://volumio.github.io/docs/Development_How_To/System_Architecture.html, weighted round robin)
- [https://dev.to/billsourour/elegant-patterns-in-modern-javascript-roro-5b5i](https://dev.to/billsourour/elegant-patterns-in-modern-javascript-roro-5b5i)
- [https://hackernoon.com/javascript-promises-and-why-async-await-wins-the-battle-4fc9d15d509f](https://hackernoon.com/javascript-promises-and-why-async-await-wins-the-battle-4fc9d15d509f)
- [https://hacks.mozilla.org/2018/03/es-modules-a-cartoon-deep-dive/](https://hacks.mozilla.org/2018/03/es-modules-a-cartoon-deep-dive/)
