---
layout: post
title: Conferencias
date: 2018-06-28 8:00:00 -0400
---

## A revisar

- [http://2018.abrelatam.org/](http://2018.abrelatam.org/)
- [http://www.lacnog.org/eventos/lacnog2018/](http://www.lacnog.org/eventos/lacnog2018/)
- [http://opendata4opencities.uji.es/](http://opendata4opencities.uji.es/)
- [https://www.geodatsci.com/](https://www.geodatsci.com/)
- [https://summit.g0v.tw/2018/](https://summit.g0v.tw/2018/)
- [https://osmc.de/](https://osmc.de/)
- [https://mobile.twitter.com/LACTLD/status/1012088034878205952](https://mobile.twitter.com/LACTLD/status/1012088034878205952)
- [https://mobile.twitter.com/OSMLatam/status/1013843274862333955 /
  https://state.osmlatam.org/ HASTA
  13/08](https://mobile.twitter.com/OSMLatam/status/1013843274862333955 /
  https://state.osmlatam.org/ HASTA 13/08)
- [https://mozillafestival.org/proposals](https://mozillafestival.org/proposals)
- [https://mobile.twitter.com/8dot8/status/1015741438729080832 HASTA
  15/08](https://mobile.twitter.com/8dot8/status/1015741438729080832 HASTA
  15/08)
- [http://www.visinpractice.rwth-aachen.de/](http://www.visinpractice.rwth-aachen.de/)
- [https://decentralizedweb.net/](https://decentralizedweb.net/)
- [https://www.puce.edu.ec/sitios/egal2019/](https://www.puce.edu.ec/sitios/egal2019/)
- [http://cplusj.org/](http://cplusj.org/)
- [http://www.lacnic.net/3011/46/evento/agenda](http://www.lacnic.net/3011/46/evento/agenda)
