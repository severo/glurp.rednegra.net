---
layout: post
title: A quién seguir en cartografía
date: 2018-06-28 8:00:00 -0400
---

- @BojanSavric: [twitter](https://twitter.com/BojanSavric)
- @recifs: [portafolio](https://illisible.net/philippe-riviere),
  [twitter](https://twitter.com/recifs), [seenthis](seenthis.net/people/fil)
- @reka: [portafolio](https://illisible.net/philippe-rekacewicz),
  [VisionsCarto](https://visionscarto.net/_philippe-rekacewicz_),
  [twitter](https://twitter.com/visionscarto),
  [seenthis](https://seenthis.net/people/reka)
- @TheMapSmith: [portafolio](https://mapsmith.net/),
  [twitter](https://twitter.com/TheMapSmith)
- @tmcw: [twitter](https://twitter.com/tmcw)

Nota importante: retiro información personal a simple solicitud.

## A revisar

- [http://www.openvisconf.com/](http://www.openvisconf.com/)
- [https://gisgeography.com/free-gis-software/](https://gisgeography.com/free-gis-software/)
- [https://carto.com/blog/twitter-guide-2018/](https://carto.com/blog/twitter-guide-2018/)
- [http://vis.cs.ucdavis.edu/vis2014papers/TVCG/papers.html](http://vis.cs.ucdavis.edu/vis2014papers/TVCG/papers.html)
- [https://twitter.com/dataviz_catalog](https://twitter.com/dataviz_catalog)
- [https://twitter.com/enkimute](https://twitter.com/enkimute)
- [http://truth-and-beauty.net/ /
  https://twitter.com/moritz_stefaner](http://truth-and-beauty.net/ /
  https://twitter.com/moritz_stefaner)
- [http://moebio.com/ / https://twitter.com/moebio](http://moebio.com/ /
  https://twitter.com/moebio)
- [http://blog.blprnt.com/ / https://twitter.com/blprnt](http://blog.blprnt.com/
  / https://twitter.com/blprnt)
- [https://stamen.com/about/who/eric-rodenbeck/ /
  https://twitter.com/ericrodenbeck](https://stamen.com/about/who/eric-rodenbeck/
  / https://twitter.com/ericrodenbeck)
- [https://mobile.twitter.com/marumushi](https://mobile.twitter.com/marumushi)
- [http://benfry.com/ / https://twitter.com/ben_fry](http://benfry.com/ /
  https://twitter.com/ben_fry)
- [http://www.bewitched.com/index.html /
  https://twitter.com/wattenberg](http://www.bewitched.com/index.html /
  https://twitter.com/wattenberg)
- [https://collabpad.io/#trgCUhN16B](https://collabpad.io/#trgCUhN16B)
- [https://mobile.twitter.com/VisInPractice/status/1017463202097926144](https://mobile.twitter.com/VisInPractice/status/1017463202097926144)
- [https://twitter.com/skotperez/status/1017417332866445319](https://twitter.com/skotperez/status/1017417332866445319)
- [https://www.mapmakerdavid.com/](https://www.mapmakerdavid.com/)
- [https://bost.ocks.org/mike/](https://bost.ocks.org/mike/)
- [https://worldmapper.org/maps/](https://worldmapper.org/maps/)
- [http://variable.io/](http://variable.io/)
- [https://pudding.cool/](https://pudding.cool/)
- [http://www.morlan.mx/](http://www.morlan.mx/)
- [https://futurememories.se/](https://futurememories.se/)
- [https://infographics.group/jobs/front-end-developer/](https://infographics.group/jobs/front-end-developer/)
- [http://www.datawheel.us/](http://www.datawheel.us/)
- [http://geomati.co/](http://geomati.co/)

Portafolios:
- [https://dataveyes.com/#!/en/lab](https://dataveyes.com/#!/en/lab)
