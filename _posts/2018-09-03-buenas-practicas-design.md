---
layout: post
title: Buenas prácticas en diseño
date: 2018-09-03 8:00:00 -0400
---

## A revisar

- [https://mobile.twitter.com/i/moments/994601867987619840](https://mobile.twitter.com/i/moments/994601867987619840)
- [https://www.readcube.com/articles/10.1201/9781315281575?shared_access_token=jNTaYvqfeqV2qwa5sUfovMvVM4Hs7mp6nzCJVzl2VoWGQrCbyQjauwd2RYb7hH61JBIbDhBHWgYFxZKHDulB9SaAw1a5aQ_jHb5Eg8Ll74Ae8RvAx6swxvf5s39QHLuU](https://www.readcube.com/articles/10.1201/9781315281575?shared_access_token=jNTaYvqfeqV2qwa5sUfovMvVM4Hs7mp6nzCJVzl2VoWGQrCbyQjauwd2RYb7hH61JBIbDhBHWgYFxZKHDulB9SaAw1a5aQ_jHb5Eg8Ll74Ae8RvAx6swxvf5s39QHLuU9)
- [http://napa-cards.net/](http://napa-cards.net/)
- [Pages - a curated gallery of web pages with a good design](https://www.pages.xyz/)
