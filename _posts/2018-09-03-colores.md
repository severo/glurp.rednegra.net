---
layout: post
title: Colores
date: 2018-09-03 8:00:00 -0400
---

## A revisar

- [https://picular.co](https://picular.co)
- [https://www.dataquest.io/blog/what-to-consider-when-choosing-colors-for-data-visualization/](https://www.dataquest.io/blog/what-to-consider-when-choosing-colors-for-data-visualization/)
- [https://coolors.co/](https://coolors.co/)
- [https://seenthis.net/messages/685076](https://seenthis.net/messages/685076)
- [https://www.colorbox.io/](https://www.colorbox.io/)
- [https://blog.datawrapper.de/colorguide/](https://blog.datawrapper.de/colorguide/)

- [colorbox - toolbox en R para manipular los colores y las paletas](http://colorspace.r-forge.r-project.org/index.html)
- [metodología para elegir los colores de base de un sitio web](https://refactoringui.com/previews/building-your-color-palette/)
- [dribbble - portfolio of designers screenshots for a particular color](https://dribbble.com/colors/a866ee.a866ee?s=popular)

Accesibilidad / color blindness:

- [https://mobile.twitter.com/\_Kcnarf/status/1039550588390965248](https://mobile.twitter.com/_Kcnarf/status/1039550588390965248)

Colores y elecciones:

- [https://en.m.wikipedia.org/wiki/Political_colour](https://en.m.wikipedia.org/wiki/Political_colour), [https://blog.datawrapper.de/partycolors/](https://blog.datawrapper.de/partycolors/)
