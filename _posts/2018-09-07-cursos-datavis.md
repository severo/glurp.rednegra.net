---
layout: post
title: Cursos de datavis
date: 2018-09-07 8:00:00 -0400
---

## Gratuitos

- Dataviz: [https://curran.github.io/dataviz-course-2018/](https://curran.github.io/dataviz-course-2018/)
- Periodismo de datos: [https://learno.net/](https://learno.net/)
- Infografía y visualización de datos: [https://x.uoc.edu/es/mooc/iniciacion-a-la-infografia-y-visualizacion-de-datos-1-a-edicion/](https://x.uoc.edu/es/mooc/iniciacion-a-la-infografia-y-visualizacion-de-datos-1-a-edicion/)
- R, Python, leaflet: [https://docs.google.com/spreadsheets/d/18GFsmSxak8U46yod0u6AtzVwIXsJBdHTHAimFwhOblw/edit?pli=1#gid=1347506605](https://docs.google.com/spreadsheets/d/18GFsmSxak8U46yod0u6AtzVwIXsJBdHTHAimFwhOblw/edit?pli=1#gid=1347506605)
- Dataviz, D3: [https://www.coursera.org/specializations/information-visualization](https://www.coursera.org/specializations/information-visualization)
- Literacia sobre visualización de datos: [https://ivmooc.cns.iu.edu/](https://ivmooc.cns.iu.edu/)
- Visualización de datos: [https://journalismcourses.org/data-viz-course-material.html](https://journalismcourses.org/data-viz-course-material.html), [http://www.thefunctionalart.com/p/instructors-guide.html](http://www.thefunctionalart.com/p/instructors-guide.html)
- Diseño de infografías: [https://www.coursera.org/learn/infographic-design/](https://www.coursera.org/learn/infographic-design/)

## De paga

- D3.js: [https://frontendmasters.com/courses/d3-js-custom-charts/](https://frontendmasters.com/courses/d3-js-custom-charts/)
