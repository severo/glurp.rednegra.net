---
layout: post
title: Fuentes de datos
date: 2018-06-28 8:00:00 -0400
---

## A revisar

- [https://eos.com/landviewer/?s=CBERS4&lat=-16.10915&lng=-61.05103&z=7&id=CBERS_4_MUX_20180321_173_121_L4&b=Red,Green,Blue](https://eos.com/landviewer/?s=CBERS4&lat=-16.10915&lng=-61.05103&z=7&id=CBERS_4_MUX_20180321_173_121_L4&b=Red,Green,Blue)
- [http://geojson.xyz/](http://geojson.xyz/)
- [https://www.indexdatabase.de/](https://www.indexdatabase.de/)
- [https://www.fmlist.org/](https://www.fmlist.org/)
- [https://whosonfirst.org/](https://whosonfirst.org/)
- [https://github.com/aourednik/historical-basemaps/](https://github.com/aourednik/historical-basemaps/)
- [https://github.com/toddmotto/public-apis](https://github.com/toddmotto/public-apis)
- [https://www.ine.gob.bo/index.php/banco/base-de-datos-sociales](https://www.ine.gob.bo/index.php/banco/base-de-datos-sociales)
- [https://www.cityscapes-dataset.com/](https://www.cityscapes-dataset.com/)
- [http://mi.eng.cam.ac.uk/research/projects/VideoRec/CamVid/](http://mi.eng.cam.ac.uk/research/projects/VideoRec/CamVid/)
- [http://download.visinf.tu-darmstadt.de/data/from_games/](http://download.visinf.tu-darmstadt.de/data/from_games/)
- [https://mobile.twitter.com/PPmerino/status/993193507711004673](https://mobile.twitter.com/PPmerino/status/993193507711004673)
- [https://blog.okfn.org/2018/02/06/open-budget-survey-2017-global-comparison-of-budget-transparency-comes-at-a-critical-time/](https://blog.okfn.org/2018/02/06/open-budget-survey-2017-global-comparison-of-budget-transparency-comes-at-a-critical-time/)
- [http://www.diva-gis.org/gdata](http://www.diva-gis.org/gdata)
- [http://sitservicios.lapaz.bo/sit/mapacultural/](http://sitservicios.lapaz.bo/sit/mapacultural/)
- [https://dados.gov.pt/pt/](https://dados.gov.pt/pt/)
- [http://datos.gt/](http://datos.gt/)
- [https://www.anh.gob.bo/index.php?N=dteyp](https://www.anh.gob.bo/index.php?N=dteyp)
- [https://mastodon.social/@Lopinel/100250558494640995](https://mastodon.social/@Lopinel/100250558494640995)
- [https://www.oecd.org/tax/tax-policy/global-revenue-statistics-database.htm](https://www.oecd.org/tax/tax-policy/global-revenue-statistics-database.htm)
- [https://adsib.gob.bo/Estadisticas-de-Dominios-bo](https://adsib.gob.bo/Estadisticas-de-Dominios-bo)
- [clasificación de municipios de Ayaviri y Alarcón](clasificación de municipios de Ayaviri y Alarcón)
- [https://mobile.twitter.com/erlanvegarios/status/1012455664571047949](https://mobile.twitter.com/erlanvegarios/status/1012455664571047949)
- [http://atesea.gob.bo/sea/index.php?option=com_content&view=article&id=36&Itemid=267](http://atesea.gob.bo/sea/index.php?option=com_content&view=article&id=36&Itemid=267)
- [http://datos.gob.sv/](http://datos.gob.sv/)
- [http://www.correo.com.uy/servicios-web/-/asset_publisher/KOZQzJts06ds/content/busqueda-de-direcciones / http://postcodes.io/](http://www.correo.com.uy/servicios-web/-/asset_publisher/KOZQzJts06ds/content/busqueda-de-direcciones / http://postcodes.io/)
- [https://www.mapx.org/](https://www.mapx.org/)
- [http://maps.eox.at/](http://maps.eox.at/)
- [http://www.citylines.co/data](http://www.citylines.co/data)
- [https://datahub.io/core/geo-countries](https://datahub.io/core/geo-countries) / [https://datahub.io/](https://datahub.io/)
- [http://www.naturalearthdata.com/downloads/10m-cultural-vectors/](http://www.naturalearthdata.com/downloads/10m-cultural-vectors/)
- [https://skymind.ai/wiki/open-datasets](https://skymind.ai/wiki/open-datasets)
- [https://wiki.openstreetmap.org/wiki/Planet.osm](https://wiki.openstreetmap.org/wiki/Planet.osm)
- [http://openoil.net/](http://openoil.net/)
- [https://www.geo.be/#!/home?l=en](https://www.geo.be/#!/home?l=en)
- [http://sim.lapaz.bo/nuevocatastro/iniciosit.htm](http://sim.lapaz.bo/nuevocatastro/iniciosit.htm)
- [https://www.cartoradio.fr/index.html#/](https://www.cartoradio.fr/index.html#/)
- [capas de base del COSIPLAN](http://mapa.ide.unasursg.org/), [otro portal con capas del COSIPLAN](https://sigcosiplan.unasursg.org/)
- [datos base de Brasil](ftp://geoftp.ibge.gov.br/cartas_e_mapas/bases_cartograficas_continuas/bc250/versao2017/shapefile/)
- [datos de limites territoriales de Francia](https://github.com/geonetwork/util-repository)

## Geocoding

- [https://adresse.data.gouv.fr/api](https://adresse.data.gouv.fr/api)
- [https://exploralat.am/](https://exploralat.am/)
- [https://radiocells.org/stats/countries](https://radiocells.org/stats/countries)
