---
layout: post
title: Procesamiento de lenguaje natural - [NLP](NLP)
date: 2018-06-28 8:00:00 -0400
---


## A revisar

- [https://mobile.twitter.com/algoritmic/status/978637106644955136](https://mobile.twitter.com/algoritmic/status/978637106644955136)
- [https://seenthis.net/messages/680816](https://seenthis.net/messages/680816)
- [https://github.com/fogleman/terrarium/blob/master/README.md](https://github.com/fogleman/terrarium/blob/master/README.md)
- [https://mobile.twitter.com/theneilrichards/status/982607288430153728](https://mobile.twitter.com/theneilrichards/status/982607288430153728)
- [https://mobile.twitter.com/theneilrichards/status/982654212491350016](https://mobile.twitter.com/theneilrichards/status/982654212491350016)
- [https://mobile.twitter.com/tjukanov/status/982619089595138050](https://mobile.twitter.com/tjukanov/status/982619089595138050)
- [https://mobile.twitter.com/geoinfinita/status/982612069047570433](https://mobile.twitter.com/geoinfinita/status/982612069047570433)
- [http://adventuresinmachinelearning.com/word2vec-tutorial-tensorflow/](http://adventuresinmachinelearning.com/word2vec-tutorial-tensorflow/)
- [https://ercim-news.ercim.eu/en100/r-i/grobid-information-extraction-from-scientific-publications](https://ercim-news.ercim.eu/en100/r-i/grobid-information-extraction-from-scientific-publications)
- [https://www.tandfonline.com/doi/full/10.1080/19312458.2018.1455817](https://www.tandfonline.com/doi/full/10.1080/19312458.2018.1455817)
- [http://nlp.fast.ai/](http://nlp.fast.ai/)
- [https://blog.dataiku.com/nlp-in-under-5-min](https://blog.dataiku.com/nlp-in-under-5-min)
- [https://mobile.twitter.com/patilindrajeets/status/1008347035832143872](https://mobile.twitter.com/patilindrajeets/status/1008347035832143872)
- [https://einstein.ai/research/the-natural-language-decathlon](https://einstein.ai/research/the-natural-language-decathlon)
- [https://mobile.twitter.com/ryanjgallag/status/1011708596122472448](https://mobile.twitter.com/ryanjgallag/status/1011708596122472448)
