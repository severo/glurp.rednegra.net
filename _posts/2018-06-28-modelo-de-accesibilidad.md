---
layout: post
title: Modelo de accesibilidad
date: 2018-06-28 8:00:00 -0400
---

## A revisar

- [https://freakonometrics.hypotheses.org/51582](https://freakonometrics.hypotheses.org/51582)
- [https://seenthis.net/messages/239783](https://seenthis.net/messages/239783)
- [https://seenthis.net/messages/683981](https://seenthis.net/messages/683981)
- [https://visionscarto.net/carte-accessibilite](https://visionscarto.net/carte-accessibilite)
- [https://beta.observablehq.com/@tmcw/isometric-real-world-terrarium-tiles](https://beta.observablehq.com/@tmcw/isometric-real-world-terrarium-tiles)
- [https://mobile.twitter.com/MapPornTweet/status/1007639000192991232](https://mobile.twitter.com/MapPornTweet/status/1007639000192991232)
- [https://www.jasondavies.com/maps/voronoi/airports/](https://www.jasondavies.com/maps/voronoi/airports/)
- [https://towardsdatascience.com/travel-time-optimization-with-machine-learning-and-genetic-algorithm-71b40a3a4c2](https://towardsdatascience.com/travel-time-optimization-with-machine-learning-and-genetic-algorithm-71b40a3a4c2)
- [https://github.com/gboeing/osmnx-examples/blob/master/notebooks/13-isolines-isochrones.ipynb](https://github.com/gboeing/osmnx-examples/blob/master/notebooks/13-isolines-isochrones.ipynb)
- [https://mobile.twitter.com/tjukanov/status/1012045288842973184](https://mobile.twitter.com/tjukanov/status/1012045288842973184)
- [https://mobile.twitter.com/root676/status/983460548225519617](https://mobile.twitter.com/root676/status/983460548225519617)
- [https://mobile.twitter.com/GeoportailWal/status/1015119403040436225](https://mobile.twitter.com/GeoportailWal/status/1015119403040436225)
- [http://www.urbalyon.org/AffichePDF/L-accessibilite*des_territoires_de_l-agglomeration_lyonnaise*-\_outils_et_scenarios-3050](http://www.urbalyon.org/AffichePDF/L-accessibilite_des_territoires_de_l-agglomeration_lyonnaise_-_outils_et_scenarios-3050)
- [http://datavizcatalogue.com/blog/isochrone-maps/](http://datavizcatalogue.com/blog/isochrone-maps/)
- [https://twitter.com/MapPornTweet/status/1016472085999177728](https://twitter.com/MapPornTweet/status/1016472085999177728)
- [https://medium.com/@tjukanov/searching-for-isolation-with-gis-eea3f2ab7d99](https://medium.com/@tjukanov/searching-for-isolation-with-gis-eea3f2ab7d99)
- [https://beta.observablehq.com/@fil/voronoi-weighted-maps](https://beta.observablehq.com/@fil/voronoi-weighted-maps)
- [http://geo.gob.bo/portal/IMG/pdf/vinculando_gente_con_pixeles_v1.pdf](http://geo.gob.bo/portal/IMG/pdf/vinculando_gente_con_pixeles_v1.pdf)
- [https://worldmapper.org/maps/grid-remoteness-2009/](https://worldmapper.org/maps/grid-remoteness-2009/)
