---
layout: post
title: Herramientas y librerías web / frontend
date: 2018-08-10 8:00:00 -0400
---

- [CSS Wizardry - Performance and Resilience: Stress-Testing Third Parties](https://csswizardry.com/2017/07/performance-and-resilience-stress-testing-third-parties/)

## Privative software

- [Charles Proxy - allows throttle to test a bad connection](https://alternativeto.net/software/charles/?license=opensource) / [also](https://anvaka.github.io/vs/?query=charles%20proxy)

## A revisar

- [http://nowodzinski.pl/syncope/](http://nowodzinski.pl/syncope/)
- [http://animista.net/play/entrances](http://animista.net/play/entrances)
- [https://bulma.io/documentation/](https://bulma.io/documentation/)
- [https://mithril.js.org/](https://mithril.js.org/) - alternativa a VueJS/React/Angular
- [https://github.com/developit/microbundle](https://github.com/developit/microbundle) - alternativa a webpack, basado en rollup
- [https://www.future-processing.pl/blog/cypress-io-the-reason-why-were-not-stuck-with-selenium/](https://www.future-processing.pl/blog/cypress-io-the-reason-why-were-not-stuck-with-selenium/)
- [https://insomnia.rest/](https://insomnia.rest/)
- [https://www.getpostman.com/](https://www.getpostman.com/)
- [https://github.com/you-dont-need/You-Dont-Need-Momentjs](https://github.com/you-dont-need/You-Dont-Need-Momentjs)
- [Descarga subconjuntos de fuentes a partir de Google Fonts](https://google-webfonts-helper.herokuapp.com/fonts/advent-pro?subsets=latin)
- [Tachyons - Framework JS y CSS](http://tachyons.io/)
- [Lossless optimization of image size](https://github.com/zevilz/zImageOptimizer)
