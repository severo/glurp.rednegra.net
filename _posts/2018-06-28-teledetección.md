---
layout: post
title: Teledetección
date: 2018-06-28 8:00:00 -0400
---

## A revisar

- [https://journals.openedition.org/cybergeo/29101](https://journals.openedition.org/cybergeo/29101)
- [https://software.nasa.gov/remotesensing/](https://software.nasa.gov/remotesensing/)
- [https://medium.com/sentinel-hub/introducing-eo-learn-ab37f2869f5c](https://medium.com/sentinel-hub/introducing-eo-learn-ab37f2869f5c)
- [https://www.ted.com/talks/will_marshall_the_mission_to_create_a_searchable_database_of_earth_s_surface](https://www.ted.com/talks/will_marshall_the_mission_to_create_a_searchable_database_of_earth_s_surface)
