---
layout: post
title: Ofertas laborales
date: 2018-06-28 8:00:00 -0400
---

## Convocatorias en Bolivia

- [ONU](http://www.oportunidades.onu.org.bo/roster/Convocatorias)
- [GIZ - Periagua](https://periagua.webmo.info/wiki/Convocatorias)
- [SICOES](https://www.sicoes.gob.bo/),
  [InfoSICOES](https://www.infosicoes.com/)
- [YPFB](http://contrataciones.ypfb.gob.bo/contrataciones/)

## Organismos internacionales y cooperación

- [PNUD](https://jobs.undp.org/cj_view_jobs.cfm)
- [UNOPS](https://jobs.unops.org/Pages/ViewVacancy/VAListing.aspx)
- [GIZ](https://www.giz.de/en/jobs/giz_job_opportunities.html)
- [OEI](http://www.oei.org.py/licitaciones/llamados)
- [Comunidad andina](http://www.comunidadandina.org/Convocatorias.aspx)

## ONG y fundaciones:

- [Oxfam](https://recruitment.oxfam.org/Vacancy.aspx?MenuID=6Dqy3cKIDOg=)
- [Ford Foundation](https://fordfoundation.wd1.myworkdayjobs.com/en-US/FordFoundationCareerPage)
- [Swiss Contact](https://www.swisscontact.org/en/about-us/jobs.html)
- [Visión Mundial](https://careers.wvi.org/job-search)

## Becas y financiamientos

- [Ford Foundation](https://www.fordfoundation.org/campaigns/public-interest-tech/individuals/)
- [Open TechnologyFund](https://www.opentech.fund/)
- [Media Democracy Fund](http://mediademocracyfund.org/)
- [FRIDA](https://programafrida.net/)

## Empresas de inteligencia artificial y Big Data:

- [Narrativa](http://www.narrativa.com/es/inicio/) -
  [anuncio](https://twitter.com/NarrativaAI/status/1003914004354469889)
- [Prevision.io](https://www.welcometothejungle.co/companies/prevision-io/jobs)
- [Kivu](http://kivu.tech/careers/)
- [OpenAI](https://jobs.lever.co/openai/)
- [Aid](https://www.aid.fr/carriere/)
- [DevelopmentSeed](https://developmentseed.org/careers/jobs/)
- [IPUMS](https://www.ipums.org/jobs.shtml)
- [CNRS-LIRIS-M2I](https://projet.liris.cnrs.fr/mi2/jobs/)

## Empresas de datavis

- [Screenful](https://screenful.com/jobs/data-visualisation-engineer)
- [DataWrapper](https://blog.datawrapper.de/hiring-engineer/)

## Trabajos y consultorías (freelance, por contrato)

- [Indeed](https://www.indeed.com/jobs?q=data&jt=contract): Global, desarrollo,
  datos, SIG, remoto, 10+ anuncios por día
- [Who is hiring](https://whoishiring.io/search/45.7060/-37.3540/2?category=programming&category=design&type=contract):
  Global, desarrollo, remoto, 3 anuncios por día, metamotor de búsqueda
- [Design Gigs for good](https://groups.google.com/forum/#!forum/design-gigs-for-good):
  Global, diseño gráfico, remoto, 3 anuncios por día
- [Data-viz-jobs](https://groups.google.com/forum/#!forum/data-vis-jobs):
  Global, mapas y dataviz, remoto, 3 anuncios por semana
- [TechnologyMasters](https://github.com/TechnologyMasters/jobs/issues?q=is%3Aopen+is%3Aissue+label%3Acontract):
  Global, desarrollo, remoto, 2 veces por mes
- [FOSS Jobs](https://www.fossjobs.net/jobs/freelance/): Global, desarrollo y
  administración con software libre, remoto, 1 anuncio por mes
- [Acolita](https://acolita.com/buscar-trabajo/#s=1): América latina, SIG,
  remoto, 2 anuncios por año
- [SwitchBoardHQ](https://geo.switchboardhq.com/): Global, SIG, remoto, 1
  anuncio por mes

## Freelancers y microtrabajo

Subcontratación / pool de "expertos":

- [Toptal](https://www.toptal.com/): Global, desarrollo, remoto, "empleado"
- [Gun.io](https://www.gun.io/): Global, desarrollo, remoto, "empleado"
- [Kolabtree](https://www.kolabtree.com/)

Freelancers:

- [Freelancer](https://www.freelancer.com/)
- [Fiverr](https://www.fiverr.com/)
- [Freelancermap](https://www.freelancermap.com/)
- [Guru](https://www.guru.com/)
- [Coworks](https://coworks.com/)
- [Upwork](https://www.upwork.com/)
- [Reddit /r/forhire](https://www.reddit.com/r/forhire/): Global, desarrollo,
  remoto, 100+ anuncios al día

Concursos:

- [Kaggle](https://www.kaggle.com/competitions): Global, ciencia de datos,
  remoto, modalidad de concurso

## Empleos y pasantías

- WOWJobs: [EEUU](https://www.wowjobs.us/), [Cánada](https://www.wowjobs.ca/):
  EEUU/Canada, TIC, presencial, +100 anuncios al día
- [Welcome to the Jungle](https://www.welcometothejungle.co/): Francia, TIC,
  presencial, +100 anuncios al día
- [LinkedIn](https://www.linkedin.com/jobs/search/?keywords=remote%20developer&location=En%20todo%20el%20mundo&locationId=OTHERS.worldwide):
  Global, desarrollo, remoto, +100 anuncios al día
- [Engineering.com](https://www.engineering.com/jobs/software-engineering/):
  EEUU/Canada, desarrollo, presencial, 20 anuncios al día
- [IwanTIC](http://iwantic.com/ofertas-de-empleo-digital/#/jobs): España,
  desarrollo, presencial, 20 anuncios al día
- [WeWorkRemotely](https://weworkremotely.com/): Global, desarrollo y sysadmin,
  remoto, 10 anuncios al día
- [RemoteOK](https://remoteok.io/): Global, desarrollo y sysadmin, remoto, 10
  anuncios al día
- [StackOverflow](https://stackoverflow.com/jobs/remote-developer-jobs?sort=p):
  Global, desarrollo, remoto, 10 anuncios al día
- [EuroEngineerJobs](https://www.euroengineerjobs.com/job_search/category/computer_engineer):
  Europa, software, presencial, 1 anuncio al día
- [FOSS Jobs](https://www.fossjobs.net/jobs/): Global, desarrollo y
  administración con software libre, remoto, 3 anuncios a la semana
- [Remote Machine Learning Jobs](https://remoteml.com/): Global, machine
  learning, remoto, 1 anuncio a la semana
- [Codepen](https://codepen.io/jobs): Global, desarrollo, remoto, pocos anuncios
- [Team OpenData](https://teamopendata.org/c/emploi/l/latest): Francia, datos
  abiertos, presencial, un anuncio al mes
