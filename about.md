---
layout: page
title: Más información
permalink: /about/
---

Aquí publico fichas de síntesis de mis temas de interés, con referencias que voy digiriendo poco a poco.

Si buscas información acerca de mi, ver [https://rednegra.net/](https://rednegra.net/sylvainlesage/en/).
